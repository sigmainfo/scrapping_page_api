FactoryGirl.define do

  factory :page_url do

    url "https://www.google.co.in"
    status "pending"
    refresh_time 10

  end
end
