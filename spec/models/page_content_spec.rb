require 'rails_helper'

RSpec.describe PageContent, type: :model do

  context 'validate page_content model' do

    it "success scenario for page_content" do
      page_url = PageUrl.create(url: 'google.com', refresh_time: 1000, status: 'pending')
      page_contents  = page_url.page_contents.create(tag_type: 'h1', content: 'he content')
      expect(page_contents).to be_valid
    end


    it "Checking url of page_content" do
      page_content_1 = FactoryGirl.build(:page_content)
      page_content_1.tag_type.equal?('h1')
    end



    it "Checking refresh time not equal of page_content" do
      page_content_1 = FactoryGirl.build(:page_content)
      page_content_1.content.equal?("h1 content here")
    end


  end

end


