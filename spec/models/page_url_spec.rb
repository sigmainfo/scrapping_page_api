require 'rails_helper'

RSpec.describe PageUrl, type: :model do

  context 'validate page_url model' do

    it "success scenario for page_url" do
      page_url_1 = FactoryGirl.build(:page_url)
      page_url_1.should be_valid
    end


    it "Checking url of page_url" do
      page_url_1 = FactoryGirl.build(:page_url)
      page_url_1.url.equal?('https://www.google.co.in')
    end



    it "Checking refresh time not equal of page_url" do
      page_url_1 = FactoryGirl.build(:page_url)
      page_url_1.refresh_time.equal?(1000)
    end


  end

end


