#  scrapping_page_api

This is a simple Ruby app using the Rails-5 framework.


## Initial Setup (Only First Time)

```
git clone https://sigmainfo@bitbucket.org/sigmainfo/scrapping_page_api.git
cd scrapping_page_api
bundle install

rake db:create
rake db:migrate
```


## Running server

```
rails s
```

Your app should now be running on [localhost:3000](http://localhost:3000/).



## Documentation



```
(1)List page url and respective page contents

GET  
http://localhost:3000/list_page_contents




(2)page url

POST
POST   http://localhost:3000/scrap_url

{"page": {"url":"http://aajtak.intoday.in/", "refresh_time":"10000"}}


refresh_time here in seconds , default value is 1000 seconds
```



## Testing

```
bundle exec rspec spec/models
bundle exec cucumber
```