require 'test_helper'

class RestApiControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get rest_api_index_url
    assert_response :success
  end

end
