class CreatePageUrls < ActiveRecord::Migration[5.0]
  def change
    create_table :page_urls do |t|
      t.string :url
      t.string :status
      t.string :refresh_time, :default => 1000

      t.timestamps
    end
  end
end
