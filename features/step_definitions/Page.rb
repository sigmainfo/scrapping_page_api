Before do
  load Rails.root + "db/seeds.rb"
end

Given(/^Should save data coming from url and save into database$/) do
  url="http://localhost:3000/scrap_url"
  res=post(url,{"page": {"url":"http://aajtak.intoday.in/", "refresh_time":"1000"}})
  parsed_body = JSON.parse(res.body)
  parsed_body.should have_content "We are scrapping your requested URL"

end

Given(/^Should update data coming from url within refresh time$/) do
  url="http://localhost:3000/scrap_url"
  res=post(url,{"page": {"url":"https://www.google.co.in", "refresh_time":"1000"}})
  parsed_body = JSON.parse(res.body)
  parsed_body.should have_content "Already scrapped few minutes ago"
  parsed_body.should have_content "https://www.google.co.in"

end

Given(/^List all Url Contents$/) do
  url="http://localhost:3000/list_page_contents"
  res=get(url)
  parsed_body = JSON.parse(res.body)
  parsed_body.should have_content "https://www.google.co.in"

end

