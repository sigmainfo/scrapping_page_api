class ApplicationJob < ActiveJob::Base

  def perform(params_url, params_refresh_time)

    url =  params_url
    agent = Mechanize.new


    page_contents = []
    existing_page_contents_ids = []
    is_page_scrapping_needed = true

    page_url =  PageUrl.find_by_url(url)

    if !page_url.nil?

      current_time = Time.now
      last_updated_time = page_url.updated_at + page_url.refresh_time.to_i

      if current_time > last_updated_time

        if !params_refresh_time.nil?
          page_url.refresh_time = params_refresh_time.to_i
          existing_page_contents_ids = page_url.page_contents.pluck :id
        else
          is_page_scrapping_needed = false
        end
      end
    else
      page_url = PageUrl.new(url: url, status: 'scrapped')
      if !params_refresh_time.nil?
        page_url.refresh_time = params_refresh_time.to_i
      end
    end

    if is_page_scrapping_needed
      begin

        page = agent.get(url)
        page.links.each {|link|
          page_contents << PageContent.new(tag_type: 'link', content: link.href)
        }

        doc = Nokogiri::HTML(page.body, "UTF-8")

        ATTR["TAGS"].each do |t|
          tag='//'+t
          doc.xpath(tag).each {|link|
            page_contents << PageContent.new(tag_type: t, content: link.text)
          }
        end

        PageUrl.transaction do
          page_url.page_contents = page_contents
          page_url.save!

          if !existing_page_contents_ids.empty?
            PageContent.delete(existing_page_contents_ids)
          end
        end
      rescue Exception => e
        puts e.message
        puts e.backtrace.inspect
      end
    end
  end
end
