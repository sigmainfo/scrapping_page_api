class PageUrl < ApplicationRecord


  has_many :page_contents, dependent: :destroy
end
